import React from "react";

const CrossBtn = React.forwardRef((props, ref) => {
  const { onClick } = props;

  return (
    <React.Fragment>
      <button
        onClick={onClick}
        ref={ref}
        aria-hidden="true"
        aria-label="Close the menu"
      >
        <div className="top" />
        <div className="bottom" />
      </button>
      <style jsx>
        {`
          button {
            border: none;
            background: none;
            color: inherit;

            visibility: hidden;
            position: absolute;
            top: calc(50% + 7px);
            left: 50%;
            width: 20px;
            height: 20px;
            transform: translateX(-50%) translateY(-50%) rotate(135deg);
            cursor: pointer;
            will-change: transform;
            backface-visibility: hidden;
            z-index: 3;
            pointer-events: auto;

            .top {
              width: 100%;
              transition-delay: 1s;
              left: 10px;
              top: 0;
              transform: rotate(90deg);
            }

            .bottom {
              width: 100%;
              transition-delay: 1.2s;
              left: 20px;
              top: 10px;
              transform: rotate(180deg);
            }
            div {
              position: absolute;
              height: 1px;
              background: #fff;
              transform-origin: left;
              z-index: 2;
              transition: 0.6s cubic-bezier(1, 0, 0, 1);
            }
          }
        `}
      </style>
    </React.Fragment>
  );
});

export default CrossBtn;
