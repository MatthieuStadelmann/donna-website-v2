import React from "react";

const PlayBtn = React.forwardRef((props, ref) => {
  const { className, onClick } = props;
  return (
    <React.Fragment>
      <button ref={ref} className={className} onClick={onClick}>
        <span />
      </button>
      <style jsx>
        {`
          button {
            height: 80px;
            width: 80px;
            border: none;
            background-color: white;
            opacity: 0.7;
            cursor: pointer;
            z-index: 998;
            border-radius: 50%;
            display: flex;
            justify-content: center;
            align-items: center;
            span {
              margin-left: 5px;
              width: 0;
              height: 0;
              border-top: 10px solid transparent;
              border-bottom: 10px solid transparent;
              border-left: 20px solid black;
            }
          }
        `}
      </style>
    </React.Fragment>
  );
});

export default PlayBtn;
