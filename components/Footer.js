const Footer = () => {
  return (
    <footer className="my-4" id={"footer"}>
      <ul>
        <li>
          <a href="https://www.instagram.com/donnazed/" target="_blank">
            Instagram
          </a>
        </li>
        <li>
          <a href="https://www.facebook.com/donnazed/" target="_blank">
            Facebook
          </a>
        </li>
        <li>
          <a href="https://www.youtube.com/user/DonnaZed" target="_blank">
            Youtube
          </a>
        </li>
        <li>
          <a
            href="https://open.spotify.com/artist/6DzNVvOIODemHlm9Uigf9l"
            target="_blank"
          >
            Spotify
          </a>
        </li>
      </ul>
      <style jsx>
        {`
          footer {
            padding-right: 40px;
            padding-left: 40px;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;

            ul {
              list-style: none;
              display: flex;
              justify-content: flex-start;
              padding: 0;
              margin: 0;
              li:not(:first-child) {
                padding-left: 0.5rem;
              }
              li {
                padding-right: 0.5rem;
                a {
                }
              }
            }
          }
        `}
      </style>
    </footer>
  );
};

export default Footer;
