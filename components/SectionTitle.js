const SectionTitle = (props) => {
  return (
    <div>
      <p className="text-bluu">{props.title}</p>
      <style jsx>
        {`
          div {
            text-align: center;
            font-size: 3rem;
            width: 100%;
            @media (max-width: 767.98px) {
              margin: 0 15px;
              text-align: center;
            }
            h2 {
              font-family: "bluu", sans-serif;
            }
          }
        `}
      </style>
    </div>
  );
};

export default SectionTitle;
