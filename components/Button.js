import React from "react";

const Button = (props) => {
  const { children, onClick } = props;

  return (
    <React.Fragment>
      <button className="draw" onClick={onClick}>
        {children}
      </button>

      <style jsx>
        {`
          button {
            font-family: bluu, sans-serif;
            padding: 0.8rem 2rem;
            color: white;
            background: none;
            letter-spacing: 1.3px;
          }
        `}
      </style>
    </React.Fragment>
  );
};

export default Button;
