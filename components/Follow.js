import React from "react";
import Icon from "./Icon";

const Follow = (props) => {
  return (
    <div className={`${props.className} follow mb-3"`}>
      {props.mobile ? null : (
        <div className="follow__title text-passive text-bluu">
          <h4>FOLLOW</h4>
        </div>
      )}

      <ul
        className={
          "d-flex justify-content-between align-items-end ml-lg-2 mb-0"
        }
      >
        <li className={"mr-2 mx-lg-2"}>
          <a
            title={"follow me on facebook"}
            href="https://www.facebook.com/donnazed/"
            target="_blank"
          >
            <Icon path={"facebook.svg"} />
          </a>
        </li>
        <li className="mx-2">
          <a
            title={"follow me on instagram"}
            href="https://www.instagram.com/donnazed/"
            target="_blank"
          >
            <Icon path={"instagram.svg"} />
          </a>
        </li>
        <li className="ml-2">
          <a
            title="follow me on youtube"
            href="https://www.youtube.com/channel/UCPIpnrgSQjUKP3Fntr8EMWA"
            target="_blank"
          >
            <Icon path={"youtube.svg"} />
          </a>
        </li>
      </ul>
      <style jsx>
        {`
          .follow {
            ul {
              list-style: none;
              padding: 0;
            }
            @media (max-width: 767.98px) {
              img {
                height: 10px;
              }
            }

            @media (min-width: 768px) {
              img {
                height: 10px;
              }
            }
            img {
              height: 20px;
            }
            &__title {
              letter-spacing: 1.74px;
              display: table-cell;
              vertical-align: bottom;
              height: 25px;
            }
          }
        `}
      </style>
    </div>
  );
};

export default Follow;
