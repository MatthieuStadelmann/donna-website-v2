import { Fragment } from "react";

const Icon = (props) => {
  const { path } = props;
  return (
    <Fragment>
      <div
        aria-hidden={true}
        dangerouslySetInnerHTML={{ __html: require(`images/${path}?include`) }}
      />
      <style jsx global>
        {``}
      </style>
    </Fragment>
  );
};
export default Icon;
