import { Fragment, useRef } from "react";
import useUnderlined from "../hooks/useUnderlined";
import Underline from "./Underline";
import Link from "next/link";

const UnderlinedLink = (props) => {
  const { className, href, active, target, linkTo } = props;
  const linkRef = useRef(null);
  const underlineRef = useRef(null);

  const { enterAnimation, leaveAnimation } = useUnderlined(
    underlineRef,
    active
  );

  if (linkTo) {
    return (
      <Fragment>
        <a
          target={"_blank"}
          onMouseOver={enterAnimation}
          onMouseLeave={leaveAnimation}
          href={linkTo}
          ref={linkRef}
          className={`js-work-link ${className}`}
        >
          <span className="js-an-word">{props.children}</span>
          <Underline ref={underlineRef} />
        </a>
        <style jsx>{`
          .js-work-link {
            text-decoration: none;
            position: relative;
          }
        `}</style>
      </Fragment>
    );
  } else {
    return (
      <Fragment>
        <Link href={href}>
          <a
            onMouseOver={enterAnimation}
            onMouseLeave={leaveAnimation}
            ref={linkRef}
            className={`js-work-link ${className}`}
          >
            <span className="js-an-word">{props.children}</span>
            <Underline ref={underlineRef} />
          </a>
        </Link>
        <style jsx>{`
          .js-work-link {
            text-decoration: none;
            position: relative;
          }
        `}</style>
      </Fragment>
    );
  }
};

export default UnderlinedLink;
