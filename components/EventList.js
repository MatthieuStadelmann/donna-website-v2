import React, { Fragment } from "react";
import moment from "moment";
import { useState } from "react";
import Button from "./Button";
import UnderlinedLink from "./UnderlinedLink";

const EventList = (props) => {
  const { events } = props;
  const [limit, setLimit] = useState(5);

  const handleClick = () => {
    if (limit < events.length) {
      setLimit(limit + 5);
    } else {
      setLimit(5);
    }
  };

  return (
    <ul>
      {events.map((event, i) => {
        const first = i === 0;
        return (
          <Fragment key={i}>
            <li
              className={
                i > limit - 1 ? "hide" : first ? "mb-4 mb-md-5" : "my-4 my-md-5"
              }
            >
              <div className={"event-date mb-1 my-md-0"}>
                <small>{moment(event.date).format("MMM Do YY")}</small>
              </div>
              <div className={"event-name  my-1 my-md-0"}>
                <UnderlinedLink linkTo={event.url} target={"_blank"}>
                  {event.name.split("(")[0]}
                </UnderlinedLink>
              </div>
              <div className={"event-location mb-4 mt-1 my-md-0"}>
                <small>{event.location.city}</small>
              </div>
            </li>
          </Fragment>
        );
      })}
      <div className={"d-flex justify-content-center"}>
        <Button onClick={handleClick}>
          {limit < events.length ? "Show More" : "Less"}
        </Button>
      </div>
      <style jsx>
        {`
          .hide {
            display: none;
          }
          ul {
            list-style: none;
            padding: 0;
          }
          li {
            display: flex;
            @media (max-width: 991.98px) {
              flex-direction: column;
              .event-date,
              .event-location,
              .event-name {
                width: 100%;
                text-align: center;
              }
            }
            @media (min-width: 992px) {
              .event-date,
              .event-location,
              .event-name {
                width: 33.333%;
              }
              .event-location {
                text-align: right;
              }
              .event-name {
                text-align: center;
                white-space: nowrap;
              }
            
            
            @media (min-width: 992px) { 
            .event-name {
            white-space: nowrap;
            
            }
             }
          }
          }
        `}
      </style>
    </ul>
  );
};

export default EventList;
