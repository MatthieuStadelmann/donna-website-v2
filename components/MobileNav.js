import React, { useRef } from "react";
import UnderlinedLink from "./UnderlinedLink";
import Follow from "./Follow";

const MobileNav = () => {
  return (
    <React.Fragment>
      <nav className="navbar navbar-expand-lg navbar-dark">
        <a className="navbar-brand text-bluu" href="#">
          Donna Zed
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">
            <li className="my-1">
              <UnderlinedLink href={"/"} className="nav-link text-bluu">
                Home
              </UnderlinedLink>
            </li>

            <li className="my-1">
              <UnderlinedLink
                target="_blank"
                linkTo={
                  "https://open.spotify.com/artist/6DzNVvOIODemHlm9Uigf9l"
                }
                className="nav-link text-bluu "
              >
                Music
              </UnderlinedLink>
            </li>

            <li className="my-1">
              <UnderlinedLink href={"/#events"} className="nav-link text-bluu">
                Events
              </UnderlinedLink>
            </li>
          </ul>
          <Follow className="d-flex justify-content-start" mobile={true} />
        </div>
      </nav>
      <style jsx>{`
        @media (min-width: 768px) {
          .navbar {
            display: none;
          }

          .navbar {
            background-color: black;
          }
        }
      `}</style>
    </React.Fragment>
  );
};

export default MobileNav;
