import MailchimpSubscribe from "react-mailchimp-subscribe";

const MailChimp = () => {
  const url =
    "https://donnazed.us20.list-manage.com/subscribe/post?u=ec0d2fcfb31edcaee67d1102c&amp;id=aa8c710e0f";

  return (
    <div className={"mailchimp-container d-flex justify-content-center my-4"}>
      <div className={"form-box"}>
        <MailchimpSubscribe url={url} />
      </div>
      <style jsx global>
        {`
          .mailchimp-container {
          }
          .form-box {
            width: 100%;

            div {
              display: flex;
              flex-direction: column;
              justify-content: center;
              input {
                margin-bottom: 1.5rem;
              }
              button {
                margin: auto;
              }
            }
          }
        `}
      </style>
    </div>
  );
};

export default MailChimp;
