import React from "react";

const BurgerMenu = React.forwardRef((props, ref) => {
  const { onClick } = props;

  return (
    <React.Fragment>
      <button
        ref={ref}
        aria-hidden="true"
        aria-label="Open the menu"
        onClick={onClick}
      >
        <div />
        <div />
        <div />
      </button>
      <style jsx>
        {`
          button {
            visibility: visible;
            position: absolute;
            top: 50%;
            left: 30px;
            width: 40px;
            cursor: pointer;
            transform-origin: left;
            transition: transform 0.9s cubic-bezier(1, 0, 0, 1);
            border: none;
            background: none;
            color: inherit;
            div:first-child {
              margin-top: 5px;
            }
            div {
              width: 100%;
              height: 1px;
              background: #fff;
              margin-bottom: 5px;
              transform-origin: left;
            }
          }
        `}
      </style>
    </React.Fragment>
  );
});

export default BurgerMenu;
