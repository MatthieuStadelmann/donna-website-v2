import React, { useRef } from "react";
import UnderlinedLink from "./UnderlinedLink";
import useUnderlined from "../hooks/useUnderlined";
import Underline from "./Underline";

const BackBtn = React.forwardRef((props, ref) => {
  const { active, onClick } = props;

  const underlineRef = useRef(null);

  const { enterAnimation, leaveAnimation } = useUnderlined(
    underlineRef,
    active
  );

  return (
    <React.Fragment>
      <button
        onClick={onClick}
        ref={ref}
        onMouseOver={enterAnimation}
        onMouseLeave={leaveAnimation}
      >
        <svg
          width="10"
          height="20"
          xmlns="http://www.w3.org/2000/svg"
          className="mr-2"
        >
          <g fill="none" fillRule="evenodd">
            <path
              d="M9.316 20a.499.499 0 01-.36-.151L.556 11.471a1.789 1.789 0 01-.522-1.267c0-.478.188-.936.522-1.267L8.957.569A.508.508 0 019.48.383a.527.527 0 01.386.411.544.544 0 01-.196.537L1.275 9.708a.701.701 0 00-.203.496c0 .187.073.366.203.496l8.396 8.378a.55.55 0 01.121.584.52.52 0 01-.476.338z"
              fill="#FFF"
              fillRule="nonzero"
            />
            <path d="M4.934 0h9.868v10.204H4.934z" />
          </g>
        </svg>
        <span className="text-bluu">Back</span>
        <Underline ref={underlineRef} />
      </button>
      <style jsx>{`
        @media (max-width: 767.98px) {
          button {
            left: 0 !important;
          }
        }
        button {
          padding: 0;
          font-size: 1.5rem;
          position: absolute;
          top: 100px;
          left: 103px;
          z-index: 998;
          background: none;
          border: none;
          color: white;
          display: none;
          align-items: center;
        }
      `}</style>
    </React.Fragment>
  );
});

export default BackBtn;
