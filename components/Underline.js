import React from "react";

const Underline = React.forwardRef((props, ref) => {
  return (
    <React.Fragment>
      <span ref={ref} className="underline" />

      <style jsx>{`
        .underline {
          display: block;
          position: absolute;
          bottom: 0;
          left: 0;
          width: 100%;
          height: calc(1px);
          background-color: white;
        }
      `}</style>
    </React.Fragment>
  );
});

export default Underline;
