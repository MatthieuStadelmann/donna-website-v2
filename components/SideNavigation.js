import { gsap } from "gsap";
import React, { useRef, useEffect, useState } from "react";
import UnderlinedLink from "./UnderlinedLink";
import BurgerMenu from "./BurgerMenu";
import CrossBtn from "./CrossBtn";
import Follow from "./Follow";

const SideNavigation = () => {
  const headerRef = useRef(null);
  const hamburgerRef = useRef(null);
  const crossRef = useRef(null);
  const logoRef = useRef(null);
  const [tl, setTl] = useState(null);

  useEffect(() => {
    let tl = gsap.timeline({ paused: true });
    tl.to(logoRef.current, {
      autoAlpha: 0,
    });
    tl.to(hamburgerRef.current, {
      autoAlpha: 0,
    });
    tl.to(crossRef.current, {
      autoAlpha: 1,
    });
    tl.to(headerRef.current, { x: 0, duration: 1, ease: "expo.out" });
    tl.from(".header__text", {
      opacity: 0,
      y: 150,
      stagger: 0.25,
    });
    setTl(tl);
  }, []);

  const openMenu = () => {
    tl.duration(1.5).play();
  };

  const closeMenu = () => {
    tl.duration(1.5).reverse();
  };

  return (
    <header className="header" ref={headerRef}>
      <div className="header__container">
        <div className="header__expanded">
          <div className="col-5 text-right d-flex flex-column justify-content-center h-100">
            <ul className="mb-3">
              <li className="my-1" onClick={closeMenu}>
                <UnderlinedLink
                  href={"/"}
                  className="header__expanded__link header__text"
                >
                  Home
                </UnderlinedLink>
              </li>

              <li className="my-1" onClick={closeMenu}>
                <UnderlinedLink
                  target="_blank"
                  linkTo={
                    "https://open.spotify.com/artist/6DzNVvOIODemHlm9Uigf9l"
                  }
                  className="header__expanded__link header__text"
                >
                  Music
                </UnderlinedLink>
              </li>

              <li className="my-1" onClick={closeMenu}>
                <UnderlinedLink
                  href={"/#events"}
                  className="header__expanded__link header__text"
                >
                  Events
                </UnderlinedLink>
              </li>
            </ul>
            <Follow className="header__text d-flex justify-content-end" />
          </div>
          <div className="col-7" />
        </div>
        <div className="header__sticky">
          <BurgerMenu ref={hamburgerRef} onClick={openMenu} />
          <CrossBtn ref={crossRef} onClick={closeMenu} />
        </div>
      </div>

      <style jsx>
        {`
          @media (max-width: 767.98px) {
            .header {
              display: none;
            }
          }

          .header,
          .header-expanded,
          .header-sticky {
            background: #000;
          }
          .header {
            position: fixed;
            top: 0;
            right: 0;
            z-index: 999;
            transform: translateX(-100%) translateX(100px);
            width: 100%;
            height: 100vh;
            &__container {
              height: 100%;
              display: flex;
            }
            ul {
              list-style: none;
              padding: 0;
            }

            &__expanded {
              width: calc(100% - 100px);
              height: 100%;
              display: flex;
              justify-content: space-between;
              background: #000;
              color: #fff;
              transform: translateZ(0);
              will-change: transform;
              backface-visibility: hidden;
              :global(&__link) {
                font-family: bluu, sans-serif;
                font-size: 4.063rem;
              }
            }
            &__sticky {
              position: relative;
              height: 100%;
              width: 100px;
              border-left: 1px solid hsla(0, 0%, 100%, 0.2);
              background: #000;

              h6 {
                transform: rotate(-90deg);
                font-family: bluu, sans-serif;
                white-space: nowrap;
                letter-spacing: 2.31px;
                position: absolute;
                bottom: 120px;
                right: -2px;
              }
            }

            &__follow {
              img {
                height: 20px;
              }
              &__title {
                letter-spacing: 1.74px;
                display: table-cell;
                vertical-align: bottom;
                height: 25px;
              }
            }
          }
        `}
      </style>
    </header>
  );
};

export default SideNavigation;
