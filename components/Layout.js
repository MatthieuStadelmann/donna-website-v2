import React from "react";
import Head from "next/head";
import SideNavigation from "./SideNavigation";
import MobileNav from "./MobileNav";
import ReactGA from "react-ga";
import { useEffect } from "react";
import donnaFbMetaImg from "../images/IMG_5431.JPG";
const Layout = (props) => {
  ReactGA.initialize("UA-133543411-1");

  useEffect(() => {
    ReactGA.pageview(window.location.pathname + window.location.search);
  }, []);

  return (
    <React.Fragment>
      <Head>
        <title>Donna Zed</title>
        <link rel="shortcut icon" type="image/png" href="/favicon.png" />
        <meta
          name="viewport"
          content="initial-scale=1.0, width=device-width"
          key="viewport"
        />

        <meta name="description" content="Swiss Artist and Model, Donna Zed" />
        <meta
          name="keywords"
          content="MUSIC, LAUSANNE, BAND, LIVE BAND, SHOW, SWITZERLAND, DONNA ZED, POP MUSIC, ROCK MUSIC, POP, CULTURE"
        />
        <meta name="author" content="Donna Zamaros" />
        <meta property="og:title" content="Donna Zed" />
        <meta property="og:description" content="Swiss Artist and Model" />
        <meta property="og:image" content={"/images/IMG_5431.JPG"} />
        <meta property="og:url" content="https://www.donnazed.com/" />
        <link
          rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
          crossOrigin="anonymous"
        />
        <script
          src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
          integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
          crossOrigin="anonymous"
        />
        <script
          src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
          integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
          crossOrigin="anonymous"
        />
        <script
          src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
          integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
          crossOrigin="anonymous"
        />
        <script
          async
          src="https://www.googletagmanager.com/gtag/js?id=UA-133543411-1"
        />
        <link
          href="https://fonts.googleapis.com/css?family=Yantramanav&display=swap"
          rel="stylesheet"
        />
      </Head>
      <MobileNav />
      <SideNavigation />
      <main>{props.children}</main>
      <style jsx global>
        {`
          body {
            background: black;
            margin: 0;
            padding: 0;
            color: white;
            font-family: "Helvetica", sans-serif;
            letter-spacing: 2px;
            overflow-x: hidden;
          }

          a,
          a:hover {
            color: white;
          }

          a:hover {
            text-decoration: none;
            cursor: pointer;
          }

          .text {
            &-passive {
              color: #5e5e5e;
            }

            &-bluu {
              font-family: "bluu", sans-serif;
            }
          }
          @media (min-width: 768px) {
            .container-fluid {
              padding-left: 100px;
            }
          }

          input {
            padding: 0.8rem 1rem;
            border: 2px solid white;
            width: 100%;
            background-color: black;
            color: white;
          }

          @media (min-width: 768px) {
            .col,
            .col-1,
            .col-10,
            .col-11,
            .col-12,
            .col-2,
            .col-3,
            .col-4,
            .col-5,
            .col-6,
            .col-7,
            .col-8,
            .col-9,
            .col-auto,
            .col-lg,
            .col-lg-1,
            .col-lg-10,
            .col-lg-11,
            .col-lg-12,
            .col-lg-2,
            .col-lg-3,
            .col-lg-4,
            .col-lg-5,
            .col-lg-6,
            .col-lg-7,
            .col-lg-8,
            .col-lg-9,
            .col-lg-auto,
            .col-md,
            .col-md-1,
            .col-md-10,
            .col-md-11,
            .col-md-12,
            .col-md-2,
            .col-md-3,
            .col-md-4,
            .col-md-5,
            .col-md-6,
            .col-md-7,
            .col-md-8,
            .col-md-9,
            .col-md-auto,
            .col-sm,
            .col-sm-1,
            .col-sm-10,
            .col-sm-11,
            .col-sm-12,
            .col-sm-2,
            .col-sm-3,
            .col-sm-4,
            .col-sm-5,
            .col-sm-6,
            .col-sm-7,
            .col-sm-8,
            .col-sm-9,
            .col-sm-auto,
            .col-xl,
            .col-xl-1,
            .col-xl-10,
            .col-xl-11,
            .col-xl-12,
            .col-xl-2,
            .col-xl-3,
            .col-xl-4,
            .col-xl-5,
            .col-xl-6,
            .col-xl-7,
            .col-xl-8,
            .col-xl-9,
            .col-xl-auto {
              padding-right: 40px;
              padding-left: 40px;
            }
          }
        `}
      </style>
    </React.Fragment>
  );
};

export default Layout;
