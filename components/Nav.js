import UnderlinedLink from "./UnderlinedLink";
import React from "react";
import { useRouter } from "next/router";
import Link from "next/link";

const Nav = React.forwardRef((props, ref) => {
  const { sections } = props;
  const router = useRouter();

  return (
    <nav ref={ref} className="navbar">
      <h1 className="text-bluu">Donna Zed</h1>
      <style jsx>{`
        @media (max-width: 767.98px) {
          .navbar {
            display: none;
          }
        }

        @media (min-width: 768px) {
          .navbar {
            top: 0;
            display: flex;
            position: absolute;
            height: 100px;
            font-size: 13px;
            justify-content: space-between;
            z-index: 998;
            width: 100%;
            padding-left: 100px;
            background-color: transparent;

            &__sideNav {
              padding: 0 0 0 85px;
              ul {
                list-style: none;
                padding: 0;
                li {
                  text-transform: capitalize;
                }
              }
            }
            :global(&__link) {
              font-size: 1.75rem;
            }

            h1 {
              position: absolute;
              left: 50%;
              padding-right: 100px;
              transform: translate(-25%, 0%);
            }
            :global(&__link) {
              font-size: 1.2rem;
            }
          }
        }
      `}</style>
    </nav>
  );
});

export default Nav;
