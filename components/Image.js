const Image = (props) => {
  const { path, alt, containerClassName, className } = props;

  return (
    <div className={`image-container ${containerClassName}`}>
      <img
        className={className}
        alt={alt}
        src={require(`images/${path}?trace`).trace}
      />
      <img className={className} src={require(`images/${path}?webp`)} />
      <style jsx>{`
        .image-container {
          position: relative;
          img {
            position: absolute;
            top: 0;
            left: 0;
          }
        }
      `}</style>
    </div>
  );
};

export default Image;
