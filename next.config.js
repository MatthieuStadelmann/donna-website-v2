const withOptimizedImages = require("next-optimized-images");
const path = require("path");
const withVideos = require("next-videos");

module.exports = withVideos(
  withOptimizedImages({
    webpack(config) {
      config.resolve.alias.images = path.join(__dirname, "images");
      return config;
    },
    env: {
      SONG_KICK_KEY: "lIBN5U9yi07fQGve",
      SONG_KICK_ARTIST_ID: 9114914,
    },
  })
);
