import React, { useEffect, useState } from "react";
import { gsap } from "gsap";

const useUnderlined = (underlineRef, active) => {
  const [tl, setTl] = useState(null);
  const [windowWidth, setWindowWidth] = useState();

  const updateWindowDimensions = () => {
    setWindowWidth(window.innerWidth);
  };

  useEffect(() => {
    window.addEventListener("resize", updateWindowDimensions);
    updateWindowDimensions();
    let tl = gsap.timeline({ paused: true });

    tl.fromTo(
      underlineRef.current,
      {
        width: active ? "100%" : "0%",
        left: "0%",
      },
      {
        width: active ? "0%" : "100%",
        left: active ? "100%" : "0%",
        duration: 1,
        ease: "expo.out",
      }
    );
    setTl(tl);

    return () => {
      window.removeEventListener("resize", updateWindowDimensions);
    };
  }, []);

  const enterAnimation = () => {
    if (windowWidth >= 768) {
      tl.play();
    }
  };

  const leaveAnimation = () => {
    if (windowWidth >= 768) {
      tl.reverse();
    }
  };

  return { enterAnimation, leaveAnimation };
};

export default useUnderlined;
