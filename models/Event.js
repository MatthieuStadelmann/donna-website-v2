export class Event {
    constructor(name, date, location, url) {
        this.name = name;
        this.date = date;
        this.location = location;
        this.url = url;
    }
}
