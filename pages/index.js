import { gsap } from "gsap";
import Layout from "../components/Layout";
import SectionTitle from "../components/SectionTitle";
import { Event } from "../models/Event";
import fetch from "isomorphic-unfetch";
import _ from "lodash";
import EventList from "../components/EventList";
import React, { useState, useEffect, useRef } from "react";
import Nav from "../components/Nav";
import UnderlinedLink from "../components/UnderlinedLink";
import PlayBtn from "../components/PlayBtn";
import YouTube from "react-youtube";
import BackBtn from "../components/BackBtn";

const Index = (props) => {
  const { futureEvents, pastEvents } = props;
  const teaserRef = useRef(null);
  const videoClipRef = useRef(null);
  const playBtnRef = useRef(null);
  const navigationRef = useRef(null);
  const backBtnRef = useRef(null);
  const [player, setPlayer] = useState(null);
  const [tl, setTl] = useState(null);
  const [backBtnTl, setBackBtnTl] = useState(null);
  const [isPaused, setIsPaused] = useState(false);

  useEffect(() => {
    let tl = gsap.timeline({ paused: true });
    let backBtnTl = gsap.timeline({ paused: true });

    tl.to(playBtnRef.current, {
      autoAlpha: 0,
      display: "none",
    });

    tl.to(navigationRef.current, {
      autoAlpha: 0,
      display: "none",
    });
    tl.to(teaserRef.current, {
      autoAlpha: 0,
      display: "none",
    });

    tl.to(videoClipRef.current, {
      autoAlpha: 1,
      display: "block",
    });

    backBtnTl.to(backBtnRef.current, {
      autoAlpha: 1,
      display: "flex",
    });

    setTl(tl);
    setBackBtnTl(backBtnTl);
  }, []);

  const opts = {
    playerVars: {
      showInfo: 0,
      modestbranding: 1,
      controls: 0,
      rel: 0,
    },
  };
  const handleReady = (event) => {
    setPlayer(event.target);
  };

  const handlePlayVideo = () => {
    tl.duration(1.5).play();
    player.playVideo();
  };

  const handlePauseVideo = () => {
    backBtnTl.duration(1).play();
    player.pauseVideo();
    setIsPaused(true);
  };

  const handleBackBtnClick = () => {
    backBtnTl.duration(0.2).reverse();
    tl.duration(1.5).reverse();
  };

  const handleStateChange = (state) => {
    if (state.data === 1 && isPaused) {
      backBtnTl.duration(0.2).reverse();
      setIsPaused(false);
    }
  };

  return (
    <Layout>
      <Nav ref={navigationRef} sections={["Home", "watch", "dates"]} />
      <BackBtn ref={backBtnRef} onClick={handleBackBtnClick} />
      <div className={"container-fluid"}>
        <div className="row pb-5 h-100">
          <div className="video">
            <div ref={videoClipRef} className="video__youtube">
              <YouTube
                videoId="qsPaCwKEkoY"
                onStateChange={handleStateChange}
                opts={opts}
                onReady={handleReady}
                onPause={handlePauseVideo}
                containerClassName={"video__youtube__container"}
                className={"video__youtube__iframe"}
              />
            </div>
            <PlayBtn
              className={"video__playBtn "}
              ref={playBtnRef}
              onClick={handlePlayVideo}
            />
            <video
              className={"video__teaser "}
              ref={teaserRef}
              autoPlay
              muted
              loop
            >
              <source
                src={require("../images/teaserinsta3.mp4")}
                type="video/mp4"
              />
            </video>
          </div>
        </div>
        <div className={"row py-5"} id="events">
          <SectionTitle title={"Future Events"} />
          <div className={"col"}>
            {!_.isEmpty(futureEvents) ? (
              <EventList events={futureEvents} />
            ) : (
              <div className={"text-center"}>
                No upcoming events,{" "}
                <UnderlinedLink
                  active={true}
                  linkTo={
                    "https://donnazed.us20.list-manage.com/subscribe/post?u=ec0d2fcfb31edcaee67d1102c&amp;id=aa8c710e0f"
                  }
                >
                  stay in touch
                </UnderlinedLink>
              </div>
            )}
          </div>
        </div>
        <div className={"row py-5"}>
          <SectionTitle title={"Past Events"} />
          <div className={"col"}>
            {!_.isEmpty(pastEvents) ? <EventList events={pastEvents} /> : null}
          </div>
        </div>
      </div>
      <style jsx>{`
        .video {
          height: auto;
          width: 100%;
          position: relative;
          top: 0;
          left: 0;
          &__teaser {
            width: 100%;
            height: auto;
          }

          :global(&__playBtn) {
            position: absolute;
            transform: translate(-50%, -50%);
            top: 50%;
            left: 50%;
            letter-spacing: 0.25rem;
          }
          &__youtube {
            display: none;
          }

          :global(&__youtube__container) {
            position: relative;
            width: 100%;
            height: 0;
            padding-bottom: 56.25%;
          }

          :global(&__youtube__iframe) {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
          }
        }
      `}</style>
    </Layout>
  );
};

Index.getInitialProps = async function () {
  const futureEventsRes = await fetch(
    `https://api.songkick.com/api/3.0/artists/${process.env.SONG_KICK_ARTIST_ID}/calendar.json?apikey=${process.env.SONG_KICK_KEY}`
  );
  const pastEventsRes = await fetch(
    `https://api.songkick.com/api/3.0/artists/${process.env.SONG_KICK_ARTIST_ID}/gigography.json?apikey=${process.env.SONG_KICK_KEY}`
  );

  const futureEvents = await futureEventsRes.json();
  const pastEvents = await pastEventsRes.json();

  return {
    futureEvents: !_.isEmpty(futureEvents.resultsPage.results)
      ? futureEvents.resultsPage.results.event.map(
          (event) =>
            new Event(
              event.displayName,
              event.start.date,
              event.location,
              event.uri
            )
        )
      : [],
    pastEvents: !_.isEmpty(pastEvents.resultsPage.results)
      ? pastEvents.resultsPage.results.event
          .reverse()
          .map(
            (event) =>
              new Event(
                event.displayName,
                event.start.date,
                event.location,
                event.uri
              )
          )
      : [],
  };
};

export default Index;
